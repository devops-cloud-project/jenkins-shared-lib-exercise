#!/usr/bin/env groovy

def incrementVersion() {
    // Increment app version in package.json
    sh 'npm version minor'

    // Read version from package.json file (plugin: pipeline utility steps)
    def packageJson = readJSON file: 'package.json'
    def version = packageJson.version

    // Set a new version as part of image name
    env.IMAGE_NAME = "$version-$BUILD_NUMBER"
    echo "Image name: ${IMAGE_NAME}"

}

def runTest() {
    echo "Testing node-project app..."
    sh 'npm install'
    sh 'npm run test'
}

def buildPushDocker() {
    echo "Build and Push docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials',
            usernameVariable: 'USER', passwordVariable: 'PASSWORD')]) {
        sh "docker build -t adik0802/node-project:${IMAGE_NAME} ."
        sh "echo $PASSWORD | docker login -u $USER --password-stdin"
        sh "docker push adik0802/node-project:${IMAGE_NAME}"
    }
}

def commitVersion() {
    echo "Commit app version back to gitlab repository"
    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials',
            usernameVariable: 'USER', passwordVariable: 'PASSWORD')]) {
        sh 'git config --global user.email "any-email-name@example.com"'
        sh 'git config --global user.name "jenkins"'

        sh 'git status'
        sh 'git branch'
        sh 'git config --list'

        sh "git remote set-url origin https://${USER}:${PASSWORD}@gitlab.com/devops-cloud-project/09-exercise-node-project-aws.git"
        sh 'git add .'
        sh 'git commit -m "Jenkins:version increment"'
        sh 'git push origin HEAD:main'
    }
}

def deployToEc2() {
    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
    def ec2Instance = "ec2-user@3.8.22.94"

    sshagent(['ec2-node-credentials']) {
        sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
    }
}